//console.log("hello world");


//HyperText Transfer Protocol - HTTP
let http = require("http"); //package
//console.log(http); - will show all the packages
//require() is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application

//http is a default packages that comes with Node.JS. it allows to use method that let us create serves
//http is a module. Modules are packages we imported
//modules are objects that contains codes, methods or data.
//the http modules let us create a server which is able to communicate with a client through the use of Hypertext transfer Protocol

//http://localhost:4000 - protocol client-erver communication - server/application



http.createServer(function(request, response){

	/*
		createServer() method is a method from the http module that aloows us to handle requests and responses from a client and a server respectively

		.createServer() method takes a function argument which is able to reeive 2 objects. 
		The request object which contains details of the request from the client.
		The response object which contains details of the response from the server.
		The createServer() method ALWAYS receives the rerquest object first before the response.
	*/
	//console.log(request.url)//contains url endpoint
	/*
		/ - default endpoint - request URL: http://localhost:4000/
		/favicon.ico - browser's default behaviour to retrieve the favicon
		/profile - request URL: http://localhost:4000/profile
		/favicon.ico
		
		**Different request require different responses
		the process or way to respond differently to a request is called a route

	*/
	//response.writeHead(200, {'Content-Type': 'text/plain'});
	/*
		response.writeHead() - is a method of the response object. it allows us to add headers to our response
		Headers are additional information about our response. we have 2 arguments in writeHead() method.
		The first is HTTP status code. an HTTP status is just a numerical code to let the client know about the status of their request.
		200 means ok, 404 means the resources cannot be found
		'Content-Type' is one of the most recognizable headers. it simply pertains to the data type of our response
		browser is limited as a server
		writeHead(status, content type)
		response.end(2500); - will not work in number. should be in STRING
	*/
	//response.end("Hello my first server!");
	/*
		response.end means end of response. also send a message/data as a string
	
		servers can repond in differently with different requests
		we start our request with our URL. a client can start a different request with a different url

		http://localhost:4000/ is not the same http://localhost:4000/profile

		/ = url endpoint (default)
		/profile = url endpoint

		we can be differentiate requests by their endpoints, we should be able to respond differently to different endpoints

		information about the url end point of the request is in the rerquet object

	*/


//specific response to a specific endpoint
//http://localhost:4000/profile - will not show as the if restricted it
if(request.url === "/"){
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end("Hello my first server! this is from / endpoint");
} else if (request.url === "/profile"){
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end("Hi Erica");
}


}).listen(4000);
/*
	.listen()allows us to assign a port to our server.
	this will allow us to serve our index.js server in our local machine assigned to port 4000
	there are several tasks and processes in our computer/machine that run on different port numbers

	http://localhost:4000/
	http - hypertext transfer protocol
	localhost - server
	port numbers:
	4000, 4040, 8000, 5000, 3000, 4200 - used for web development

	console.log() is added to show validation which port number our server is running on
	in fact, the more complex our servers become, the more 
*/

console.log("Server is running in localHost:4000!");